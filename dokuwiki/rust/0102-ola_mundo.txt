**A Linguagem de Programação Rust**

[[:rust:0101-instalacao|Instalação]] | [[:rust|Índice]] | [[:rust:0103-ola_cargo|Olá, Cargo!]]

----

====== Olá, mundo! ======


Agora que você já instalou o Rust, vamos escrever o seu primeiro programa em Rust. É tradicional, quando se aprende uma nova linguagem, escrever um pequeno programa que exibe o texto ''Olá, mundo!'' na tela. Então, faremos o mesmo aqui!

<note>Este livro presume uma familiaridade básico com a linha de comando. O Rust não faz nenhuma exigência específica quanto às suas ferramentas de edição ou onde o seu código deve ficar. Então, se você preferir uma IDE, em vez da linha de comando, fique à vontade para utilizar a sua favorita. Muitas IDE's possuem algum nível de suporte ao Rust, verifique a documentação da sua para mais detalhes. Recentemente, a equipe do Rust tem se concentrado em possibilitar um ótimo suporte para IDE's, e tem havido um progresso rápido nessa frente!</note>

===== Criando uma pasta de projetos =====

Você vai começar criando um diretório para guardar os seus códigos em Rust. Para o Rust, não importa onde o código está. Mas, para os exercícios e projetos deste livro, nós sugerimos criar uma pasta ''projetos'' no seu diretório ''home'' e manter todos os seus projetos ali. 

Abra um terminal e digite os seguintes comandos para criar a pasta ''projetos'' e uma pasta para o projeto ''Olá, mundo!'' dentro dela.

No Linux, macOS e PowerShell, no Windows, digite isso:

<code>
$ mkdir ~/projetos
$ cd ~/projetos
$ mkdir ola_mundo
$ cd ola_mundo
</code>

No CMD do Windows, digite isso:

<code>
> mkdir "%USERPROFILE%\projetos"
> cd /d "%USERPROFILE%\projetos"
> mkdir ola_mundo
> cd ola_mundo
</code>

===== Escrevendo e executando um programa em Rust =====

Em seguida, crie um novo arquivo fonte chamado ''main.rs''. Os arquivos em Rust sempre terminam com a extensão ''.rs''. Se o nome do arquivo tiver mais de uma palavra, utilize o caractere sublinhado (''_'') para separá-las. Por exemplo, use ''ola_mundo.rs'' em vez de ''olamundo.rs''.

Agora, abra o arquivo ''main.rs'' e digite nele o código abaixo:

<code rust main.rs>
fn main() {
    println!("Olá, mundo!");
}
</code>

Salve o arquivo e volte à janela do terminal. No Linux ou no macOS, digite os seguintes comandos para compilar e executar o arquivo:

<code>
$ rustc main.rs
$ ./main
Olá, mundo!
</code>

No Windows, execute com ''.\main.exe'' em vez de ''./main'':

<code>
> rustc main.rs
> .\main.exe
Hello, world!
</code>

Independente do seu sistema operacional, a string ''Olá, mundo!'' deve ser exibida no terminal. Se você não vir esta saídas, volte ao tópico [[rust:0101-instalacao#solucao_de_problemas|"Solução de problemas"]] da seção sobre a instalação para ver como obter ajuda.

Se ''Olá, mundo!'' for exibido, parabéns! Você escreveu oficialmente o seu primeiro programa em Rust. Isso faz de você um programador em Rust -- seja bem-vindo!

===== Anatomia de um programa em Rust =====

Vamos rever em detalhes o que aconteceu no seu programa "Olá, mundo!". Aqui está a primeira peça do quebra-cabeça:

<code rust>
fn main() {

}
</code>

Essas linhas definem uma //**função**// no Rust. A função ''main'' é especial: ela é sempre o primeiro código a rodar em qualquer programa executável em Rust. A primeira linha declara uma função chamada ''main'', que não tem parâmetros e não retorna nada. Se houvesse parâmetros, eles iriam dentro dos parêntesis, ''()''.

Note, também, que o corpo da função é envolvido por chaves, ''{}''. O Rust exige isso em volta de todos os corpos de funções. É uma boa prática de estilo colocar a chave de abertura na mesma linha da declaração da função com um espaço entre elas.

No momento em que este livro está sendo escrito, uma ferramenta de formatação automática, chamada ''rustfmt'' está em desenvolvimento. Se você quiser se manter no estilo padrão em todos os projetos em Rust, o ''rustfmt'' formatará o seu código em um determinado estilo. A equipe do Rust planeja incluir essa ferramenta na distribuição padrão do Rust, como o ''rustc''. Então, dependendo de quando você está lendo este livro, ela já pode estar instalada no seu computador! Verifique a documentação online para mais detalhes.

<note tip>**Nota da tradução:** esta tradução foi feita quando a versão estável do Rust era a 1.41.0, de 27 de janeiro de 2020, e a ferramenta ''rustfmt'' já estava incluída na instalação.</note>

Dentro da função ''main'', nós temos o seguinte código>

<code rust>
    println!("Olá, mundo!");
</code>

Esta linha faz todo o trabalho neste pequeno programa: ela exibe o texto na tela. Existem quatro detalhes importantes a serem notados aqui. Primeiro, o estilo do Rust é para ser indentado com quatro espaços, não uma tabulação.

Segundo, ''println!'' chama uma //**macro**// do Rust. Se chamasse uma função, teria sido digitado como ''println'' (sem a ''!''). Nós discutiremos as macros em mais detalhes no Capítulo 19. Por enquanto, você só precisa saber que usar a ''!'' significa que você está chamando uma macro em vez de uma função normal.

Terceiro, você está vendo a string ''"Olá, mundo!"''. Nós passamos esta string como um argumento de ''println!'' e ela é exibida na tela.

Quarto, nós terminamos a linha com um ponto e vírgula ('';''), o que indica que a expressão foi concluída e a próxima está pronta para começar. A maioria das linhas de um código em Rust terminam com ponto e vírgula.

===== Compilar e executar são passos separados =====

Você já executou um programa recém-criado. Então, vamos examinar cada passo do processo.

Antes de executar um programa em Rust, você deve compilá-lo usando o compilador Rust, através do comando ''rustc'' seguido no nome do arquivo fonte, assim:

<code>
$ rustc main.rs
</code>

Se você tem experiência com C ou C++, você notará que isso é semelhante ao ''gcc'' ou ao ''clang''. Depois de compilar com sucesso, o Rust entrega um binário executável.

No Linux, macOS e no PowerShell do Windows, você pode ver o executável digitando o comando ''ls'' no seu shell. No Linux e no macOS, você verá dois arquivos. Com o PowerShell do Windows, você verá os mesmos três arquivos que veria usando o CMD.

<code>
$ ls
main  main.rs
</code>

No CMD do Windows, você digitaria o seguinte:

<code>
> dir /B %= a opção /B manda mostrar apenas os nomes de arquivos =%
main.exe
main.pdb
main.rs
</code>

Isso mostra o arquivo fonte, com a extensão ''.rs'', o arquivo executável (''main.exe'' no Windows, mas apenas ''main'' em outras plataformas) e, no Windows, um arquivo contendo informações de debug com a extensão ''.pdb''. Daqui, você executa o arquivo ''main'' ou ''main.exe'' assim:

<code>
$ ./main # ou .\main.exe no Windows
</code>

Se ''main.rs'' for o seu programa "Olá, mundo!", esta linha exibirá ''Olá, mundo!'' no seu terminal.

Se você estiver mais familiarizado com uma linguagem dinâmica, como Ruby, Python ou JavaScript, você pode não estar acostumado a compilar e executar programas em etapas diferentes. O Rust é uma linguagem //previamente compilada//, significando que você pode compilar um programa e dar o executável para outra pessoa, e ela poderá executá-lo mesmo sem ter o Rust instalado. Se você der um arquivo ''.rb'', ''.py'' ou ''.js'', ela irá precisar de uma implementação do Ruby, do Python ou do JavaScript (respectivamente) instalada. Mas, nessas linguagens, você só precisa de um comando para compilar e executar o seu programa. Tudo é uma troca no design de linguagens.

Apenas compilar com o ''rustc'' funciona para programas mais simples. Mas, à medida que o seu projeto cresce, você terá que administrar todas as opções e facilitar o compartilhamento do seu código. A seguir, vamos apresentar a ferramenta //**Cargo**//, que irá ajudar você a escrever programas reais em Rust.


----

**A Linguagem de Programação Rust**

[[:rust:0101-instalacao|Instalação]] | [[:rust|Índice]] | [[:rust:0103-ola_cargo|Olá, Cargo!]]

----

Este material está sendo oferecido gratuitamente e eu ainda pretendo disponibilizar outros. Então, se quiser me dar uma força para continuar fazendo esse trabalho...

[[https://pag.ae/7Vu5Cocjo|{{https://stc.pagseguro.uol.com.br/public/img/botoes/doacoes/209x48-doar-assina.gif}}]]

Desde já, muito obrigado!

----

Comente esta página no [[https://forum.debxp.org/|nosso fórum]]!


~~NOTOC~~
~~NOCACHE~~
